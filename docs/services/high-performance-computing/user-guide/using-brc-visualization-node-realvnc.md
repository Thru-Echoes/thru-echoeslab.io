---
title: Using the BRC Visualization Node with RealVNC
keywords: high performance computing
tags: [hpc]
---

# Using Viz Node with RealVNC

!!! summary
    The purpose of this document is to explain how to set up and use a graphical user interface (GUI) desktop environment on Savio.

    The remote desktop allows users to run a real desktop within the cluster environment. This is very useful for users who require to run scientific applications with a GUI seamlessly on the cluster, especially some commercial applications, or visualization applications to render results. It also allows users to disconnect and resume from anywhere without losing the work. The remote desktop service is provided with the <a href="https://www.realvnc.com/" target="_blank">RealVNC</a> solution.

## Usage Instructions

<p>To access the service please follow these steps:</p>
<ol><li>Download and install the RealVNC viewer for your platform of choice.
<ul><li>Download the VNC Viewer from <a href="https://www.realvnc.com/en/connect/download/viewer/" target="_blank">here</a>.</li>
<li>Alternatively, you can download a VNC Viewer extension for the Chrome browser <a href="https://chrome.google.com/webstore/detail/vnc%C2%AE-viewer-for-google-ch/iabmpiboiopbgfabjmgeedhcmjenhbla?hl=en" target="_blank">here</a> to launch the viewer directly from the Chrome browser.</li>
</ul></li>
<li>Start the VNC server on the dedicated viz node and record your DISPLAY ID.
<ul><li>If you are connecting from within the cluster, for example, if you already have a login session on one of the login nodes, please use the internal hostname, i.e.,<br>
&nbsp;&nbsp;&nbsp;<code>ssh viz</code></li>
<li>If you are connecting from outside of the cluster, please use the external hostname, i.e.,<br>
&nbsp;&nbsp;&nbsp;<code>ssh &lt;username&gt;<username>@viz.brc.berkeley.edu</username></code></li>
<li>Once on the viz node, start the VNC server with “<code>vncserver</code>”. You should see something like:<br><code>-bash-4.1$ vncserver<br>
VNC(R) Server 6.2.0 (r29523) x64 (Aug 3 2017 17:27:11)<br>
Copyright (C) 2002-2017 RealVNC Ltd.<br>
RealVNC and VNC are trademarks of RealVNC Ltd and are protected by trademark<br>
registrations and/or pending trademark applications in the European Union,<br>
United States of America and other jurisdictions.<br>
Protected by UK patent 2481870; US patent 8760366; EU patent 2652951.<br>
See https://www.realvnc.com for information on VNC.<br>
For third party acknowledgements see:<br>
https://www.realvnc.com/docs/6/foss.html</code>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<p><code>On some distributions (in particular Red Hat), you may get a better experience<br>
by running vncserver-virtual in conjunction with the system Xorg server, rather<br>
than the old version built-in to Xvnc. More desktop environments and<br>
applications will likely be compatible. For more information on this alternative<br>
implementation, please see: https://www.realvnc.com/doclink/kb-546</code></p>
<p><code>Running applications in /etc/vnc/xstartup.custom</code></p>
<p><code>VNC Server catchphrase: "House compass horizon. Betty lark Pacific."<br>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; signature: 44-d7-58-35-1e-9e-2f-53</code></p>
<p><code>Log file is /global/home/users/&lt;username&gt;<user>/.vnc/viz.brc:4.log<br>
New desktop is viz.brc:4 (136.152.224.36:4)</user></code></p></li>
<li>This last line contains the port number (your DISPLAY ID) that you will use to connect to this vncserver that you have just launched, <strong>please remember it.</strong></li>
<li>In the above example&nbsp;output, the DISPLAY ID is 4.</li>
</ul></li>
<li>Connect to the VNC server with VNC Viewer on your local platform.
<ul><li>Launch VNC Viewer or launch Chrome and start the VNC Viewer extension.</li>
<li>If you are using the VNC viewer application, you should see:<img alt="VNC viewer application window" style="height: 543px; width: 800px;" class="media-element file-default image-style-none" data-delta="1" typeof="foaf:Image" src="{{ site.baseurl }}/images/image15.png" width="1774" height="1203"></li>
<li>If you are using the VNC Chrome extension, you should see:<img alt="VNC Chrome extension window" style="height: 626px; width: 800px;" class="media-element file-default image-style-none" data-delta="2" typeof="foaf:Image" src="{{ site.baseurl }}/images/image5.png" width="1999" height="1564"></li>
<li>In the “VNC Server” field at the top of the screen, please enter "viz.brc.berkeley.edu:DISPLAY" (without quotes) and replace "DISPLAY" with the actual DISPLAY ID that you obtained from the last step, for example, viz.brc.berkeley.edu:<strong>4</strong>. You can leave the other options on their default settings. Now, please press the ENTER key or the CONNECT button.</li>
<li>After clicking the "Connect" button, a window should pop up to prompt you for your username and password:<br><img alt="Username and Password window" style="height: 205px; width: 300px;" class="media-element file-default image-style-none" data-delta="3" typeof="foaf:Image" src="{{ site.baseurl }}/images/image14.png" width="873" height="597"></li>
<li>Please fill in your cluster username and the PIN+password generated from your OTP device the same way you would to <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/logging-brc-clusters">log into Savio</a>. Check that the “signature” matches the one in the output of the <code>vncserver</code> command. If it does not match, you may be using the wrong DISPLAY number.</li>
</ul></li>
</ol><p>After following these steps you should be able to see a VNC Viewer window with an xterm window running, it will look like this:</p>
<p><img alt="VNC Viewer window" style="height: 575px; width: 800px;" class="media-element file-default image-style-none" data-delta="4" typeof="foaf:Image" src="{{ site.baseurl }}/images/image2.png" width="1172" height="842"></p>
<p>This is the default environment and you can start running your GUI applications.</p>
<p>If the desktop is too small for your application, you can change the geometry when starting the VNC server. The proper option is <code>-geometry &lt;Width&gt;x&lt;Height&gt;</code>, e.g., <code>vncserver -geometry 1024x768</code>.</p>

## Program Examples

<p>Typically, programs need to be started up from the command line. As with batch and interactive jobs, the appropriate modules must be loaded before the program can be launched. See below for examples of starting VisIt and matplotlib.</p>

### Starting a Web Browser

<p>You can start a browser by running either of the following from an xterm window:</p>
<p><font face="monospace"><span style="font-size: 10.569px;">firefox</span></font></p>
<p><font face="monospace"><span style="font-size: 10.569px;">otterbrowser</span></font></p>

### Starting Visit

<p>To use VisIt, load the VisIt module with the command <code>module load visit</code> and then start the program with the command <code>visit</code> as shown in the following screenshots:</p>
<p><img alt="VisIt commands" style="height: 337px; width: 500px;" class="media-element file-default image-style-none" data-delta="5" typeof="foaf:Image" src="{{ site.baseurl }}/images/image13.png" width="1004" height="676"></p>
<p><img alt="VisIt GUI windows" style="height: 600px; width: 800px;" class="media-element file-default image-style-none" data-delta="6" typeof="foaf:Image" src="{{ site.baseurl }}/images/image16.png" width="1999" height="1499"></p>

### Using Matplotlib

<p>Matplotlib can be used to plot and display figures, but it must be done from within Python.<br>
First, load your desired Python version with <code>module load python/x.y</code> (where <code>x.y</code> is replaced with the appropriate version number). Then, start Python and import matplotlib as desired. Example commands are shown in the following screenshot.</p>
<p><img alt="Python matplotlib commands and window" style="height: 622px; width: 800px;" class="media-element file-default image-style-none" data-delta="7" typeof="foaf:Image" src="{{ site.baseurl }}/images/python_0.png" width="1024" height="796"></p>

## Ending the Session

<p>When you are done using the remote desktop, please shut it down properly to release resources for other users. Simply closing the VNC Viewer does not clean the resource on the server. To properly do so, if you are using the Openbox window manager, right click at an empty space within the VNC Viewer window, then click "Log Out" and the vncserver will be shut down:</p>
<p><img style="height: 655px; width: 800px;" class="media-element file-default image-style-none" data-delta="8" typeof="foaf:Image" src="{{ site.baseurl }}/images/image3.png" width="1028" height="842" alt=""></p>
<p>Alternatively, to end your vncserver session from within the BRC infrastructure, log back onto <code>viz.brc.berkeley.edu</code> and enter the command</p>
<p><code>vncserver -kill :&lt;port number&gt;</code></p>
<p>Following the example given above, this command would be:</p>
<p><code>vncserver -kill :4</code></p>
<p>If you have any questions about using the visualization node or run into any issues in the process of using the visualization node, please contact us at&nbsp;<a href="mailto:brc-hpc-help@berkeley.edu">brc-hpc-help@berkeley.edu</a>.</p>
