---
title: Making files accessible to group members and other Savio users
keywords: high performance computing, berkeley research computing
tags: [hpc]
---

# Making Files Accessible 

!!! summary 
    <p>This page documents some aspects of file permissions on Linux and some tools you can use to make files accessible to your group members or more broadly to other Savio users.</p>

<div class="toc-filter-back-to-top first"><a href="#top" class="toc-filter-processed">Back to top</a></div>

## Background on Linux file permissions and file ownership

<p>We'll start by briefly describing how to understand the permissions and ownership of files and directories using the 'long' version<br>
of the <code>ls</code> command.</p>
<p><code>[paciorek@ln002 ~]$ ls -l<br>
-rw-r----- 1 paciorek ucb 0 May 6 16:39 file1<br>
-rw-rw-rw- 1 paciorek ucb 0 May 6 16:39 file2<br>
drwxr-xr-x 1 paciorek ucb 0 May 6 16:39 dir1</code></p>
<p>The first set of 10 digits shows whether an item is a directory (see the 'd' for <code>dir1</code>) or not and then the read/write/execute<br>
permissions, first for the user, then for the group and then for others. Here we see that <code>file1</code> is readable and writeable by the user, but only readable by the group and not accessible to others. In contrast <code>file2</code> is readable and writeable by everyone.</p>
<p>To see what groups you belong to and the permissions on your home and scratch directories, simply do this:<br><code>[paciorek@ln001 ~]$ groups<br>
ucb savio matlab co_stat fc_paciorek<br>
[paciorek@ln001 ~]$ ls -ld ~paciorek<br>
drwxr-xr-x 1 paciorek ucb 65536 Apr 30 14:08 /global/home/users/paciorek<br>
[paciorek@ln001 ~]$ ls -ld /global/scratch/paciorek<br>
drwxr-xr-x 22 paciorek ucb 4096 Apr 10 14:47 /global/scratch/paciorek</code></p>
<p>We can see that the user belongs to a Condo group and to an FCA group, as well as to some broader groups.<br>
Also note that your home and scratch directories are part of a generic group ('ucb');and not part of a Condo or FCA group.</p>
<p>Here are some links to more information on <a href="https://en.wikipedia.org/wiki/File_system_permissions#Traditional_Unix_permissions" class="toc-filter-processed" target="_blank">UNIX file permissions</a> and <a href="https://en.wikipedia.org/wiki/Chmod#Symbolic_modes" class="toc-filter-processed" target="_blank">interpreting the permission modes</a>.<br>
And remember you can also use <code>man</code> to get help on the various commands discussed here.<br><code>man chmod<br>
man chown<br>
man chgrp</code></p>
<div class="toc-filter-back-to-top"><a href="#top" class="toc-filter-processed">Back to top</a></div>

## Using a group directory

<p>You can ask for a group directory in <code>/global/home/groups</code> to be made available for your Condo or FCA by contacting <a href="mailto:brc-hpc-help@berkeley.edu">brc-hpc-help@berkeley.edu</a>.</p>
<p>Any files created in the group directory will generally&nbsp;belong to that group and will generally be readable by the group.</p>
<p>To make a file writeable by the group:<br><code>cd /global/home/groups/somegroup/some_directory<br>
chmod g+w myfile</code></p>
<div class="toc-filter-back-to-top"><a href="#top" class="toc-filter-processed">Back to top</a></div>

## Making files accessible to all other Savio users

<p>Here is some template code showing how to make files in your home directory available to all Savio users. Note that for a file in a directory to be available for access, the directory in which the file lives and all directories above that need to be executable by other users. Then you can individually set permissions on files in the directory to decide which files to make accessible and whether they are readable, writeable, or executable by other users.</p>
<p><code>chmod go+X ~username # make your home directory accessible to all users<br>
chmod go+X ~username/mydir # make a subdirectory accessible to all users<br>
chmod go+r ~username/mydir/myfile # make the file readable by other users<br>
# at this point other users cannot view the list of files in ~username or ~username/mydir<br>
chmod go+r ~username/mydir # now other users can do "ls ~username/mydir"</code></p>
<p>One can make an entire set of files and directories accessible using the recursive (-R) flag:<br><code>chmod -R go+X ~username/mydir # allow access to mydir and all its subdirectories<br>
chmod go+r ~username/mydir/myfile # now allow read access to myfile specifically<br>
chmod -R go+rX ~username/mydir # allow read access to all files in mydir and its subdirectories</code></p>
<p>To make files in your scratch directory accessible, simply change <code>~username</code> to <code>/global/scratch/username</code> above.</p>
<div class="toc-filter-back-to-top"><a href="#top" class="toc-filter-processed">Back to top</a></div>

## Making files accessible to your group members

<p>As noted above, by default the group associated with anything in your home or scratch directory is a general group and not your FCA or condo group. So you probably first want to make a given directory owned by the (FCA or condo) group of interest. The next lines do that and ensure that future files/directories will also be in 'somegroup'.</p>
<p><code>chown username:somegroup ~username/mydir # now mydir is in 'somegroup'<br>
chmod g+s mydir # now future files/directories created in mydir will belong to 'somegroup'<br>
chmod -R g+s mydir # future files/directories created in any existing subdirectories will also belong to 'somegroup'<br>
chmod go+X ~username # need to open to all users at top level since ~username is not in 'somegroup'<br>
chmod g+rwX ~username/mydir # make the directory accessible to group members</code></p>
<p>Now if you already have files in 'mydir' that you want accessible to the group, you need to modify the files, e.g.,</p>
<p><code>chown username:somegroup ~username/mydir/myfile # make a pre-existing file owned by the group<br>
chmod g+rw ~username/mydir/myfile # make a pre-existing file readable and writeable to the group members</code></p>
<p>For recursive changes see the commands in the previous section, but change "go" to just "g".</p>
<p>Now group members should be able to read, write, create, delete, and execute files within the directory.<br></p><div class="toc-filter-back-to-top last"><a href="#top" class="toc-filter-processed">Back to top</a></div>
