---
title: Using Authy on a desktop computer to generate one-time passwords for Savio
keywords: high performance computing, berkeley research computing
tags: [hpc]
---

# Using Authy and Generating OTP

!!! summary
    <a href="https://www.authy.com/app/" target="_blank">Authy</a>, a free app from Twilio, can generate one-time passwords (OTPs) on your laptop or desktop computer, which you can use when logging into the <a href="{{ site.baseurl }}/services/high-performance-computing">Savio</a> high-performance computing cluster at UC Berkeley.

    We recommend using the Authy app if you do not have a mobile device - such as most iOS or Android smartphones and tablets - capable of running the Google Authenticator app. (If you do have one of these, you can find setup instructions for your device in <a href="{{ site.baseurl }}/services/high-performance-computing/logging-brc-clusters">Logging into Savio</a>.)

    Authy is a <a href="https://developer.chrome.com/apps/about_apps" target="_blank">Chrome app</a>; that is, it requires the presence of the Chrome browser but works more like a desktop app than a browser extension. This app can be installed and used wherever Chrome runs, including on MS Windows, OS X / macOS, and Linux.

### Setting up Authy

<p>Setup steps:</p>
<ol><li>Open (or switch to) the Chrome browser. (If you don't have that browser on your device, visit <a href="https://www.google.com/chrome/" target="_blank">Google's Chrome Browser page</a> to download and install it.)</li>
<li>From within Chrome, <a href="https://www.authy.com/app/desktop/" target="_blank">download Authy</a>.
<p>In the following section of the download page (<a href="https://www.authy.com/app/desktop/" target="_blank">https://www.authy.com/app/desktop/</a>), click the icon for the OS you're using:</p>
<p><img alt="Screenshot: download icons on Authy website" height="222" width="544" class="media-element file-default image-style-none" data-delta="1" typeof="foaf:Image" src="{{ site.baseurl }}/images/2-authy-download.png"></p></li>
<li>Then, on the Authy app page in the Chrome Web Store, click the '+ ADD TO CHROME' button near top right, and 'Add app' in the subsequent dialog.</li>
<li>Visit your browser's Apps page, by entering <code>chrome://apps/</code> in your browser's address bar and pressing the Return key. (You can also bookmark that page, for convenience.)</li>
<li>Launch Authy by clicking its icon on your browser's Apps page:<br><img alt="Authy app icon on Chrome browser apps screen" height="367" width="549" class="media-element file-default image-style-none" data-delta="1" typeof="foaf:Image" src="{{ site.baseurl }}/images/5-authy-chrome-apps-screen.png"></li>
<li>Follow the on-screen instructions to send a text message to your phone, or to call you. This will give you a code that you will need to enter into Authy.
<p>(If you have a landline phone, or a cell phone that can't receive text messages, be sure to click the "Call" button - rather than the "SMS" button - at the "Verify my identity via" prompt.)</p></li>
<li>Next, in Authy's window, click the "Settings" (gear) icon at upper left:
<p><img alt="Screenshot: Authy 'gear' (settings) icon on Add Account screen" height="588" width="320" class="media-element file-default image-style-none" data-delta="1" typeof="foaf:Image" src="{{ site.baseurl }}/images/7-authy-add-account.png"></p></li>
<li>Set up a Master password by entering text into that field, clicking the "Set" link, and then following the onscreen instructions:
<p><img alt="Screenshot: Authy master password screen" height="506" width="546" class="media-element file-default image-style-none" data-delta="1" typeof="foaf:Image" src="{{ site.baseurl }}/images/8-authy-master-password.png"></p></li>
<li>Click the Close ('x') button at upper right to get back to the Accounts screen.</li>
<li>Click the red Plus ('+') button on that screen to create a new account. You'll now see a screen asking for you to enter a code:
<p><img alt="Screenshot: Authy New Authenticator Account screen, with Enter Code field" height="505" width="545" class="media-element file-default image-style-none" data-delta="1" typeof="foaf:Image" src="{{ site.baseurl }}/images/10-authy-new-acct.png"></p></li>
<li>Now you'll leave Authy, go back to your main Chrome window, and get that code. Here's how to do that:
<p>Visit the <a href="https://identity.lbl.gov/otptokens/hpccluster" target="_blank">Non-LBL Token Management</a> web page (<a href="https://identity.lbl.gov/otptokens/hpccluster" target="_blank">https://identity.lbl.gov/otptokens/hpccluster</a>).</p></li>
<li>Login to that web page by clicking the button for the external account (UC Berkeley CalNet, Google, Facebook, or LinkedIn) that you previously linked to your Savio/BRC cluster account.
<p><em>(If, when doing so, you encounter the error message, "<span class="im">Login Error: There was an error logging you in. The account that you logged in with has not been mapped to a system account", please complete Step 1, on linking your personal account to a BRC cluster account, in <a href="{{ site.baseurl }}/services/high-performance-computing/logging-brc-clusters#Installing" class="toc-filter-processed">Logging into BRC Clusters</a>. Then, return right back here, to re-try this step in the Authy instructions.)</span></em></p></li>
<li>From the "Token Management" page which appears, create a new token by clicking on "Add an HPC Cluster/Linux Workstation token" and following the onscreen instructions.
<p>IMPORTANT: Remember the PIN that you are setting on the token.</p>
<p>Note: Even if you've already created one or more tokens for use with Google Authenticator on a smartphone or tablet, you'll still need to create a new token for use with Authy.</p></li>
<li>After you've successfully created your new token, a QR code for that token will then be displayed.
<p><img alt="Screenshot: QR code for one-time password token" height="272" width="632" class="media-element file-default image-style-none" data-delta="1" typeof="foaf:Image" src="{{ site.baseurl }}/images/14-authy-qrcode.png"></p></li>
<li>Because Authy doesn't have a way to scan the QR code (directly or via a helper app), you'll need to extract the 'secret' from the currently displayed webpage.
<p><em>(The instructions that follow here are a bit tricky ("fiddly") so please be sure to pay close attention to both the instructions and screenshots.)</em></p>
<p>To do so, from Chrome's menus, choose "View -&gt; Developer ... -&gt; Developer Tools".</p></li>
<li>Click the "Inspect Element" icon - the icon with the 'arrow in a box,' at the upper left of the right-hand panel.
<p><img alt="Screenshot: Inspect Element widget icon in Chrome's developer tools" height="209" width="996" class="media-element file-default image-style-none" data-delta="1" typeof="foaf:Image" src="{{ site.baseurl }}/images/16-authy-inspect-element.png"></p></li>
<li>Then click on the QR code, so that code is highlighted:
<p><img alt="Screenshot: clicking on QR code image using Inspect Element tool" height="396" width="1267" class="media-element file-default image-style-none" data-delta="1" typeof="foaf:Image" src="{{ site.baseurl }}/images/17-authy-click-qrcode.png"></p></li>
<li>Over in the right-hand panel, you'll see some text highlighted, which will most likely begin with <code>img style=....</code> (That's the HTML markup which corresponds to the image of the QR code, in the left-hand panel.)
<p>Press the up arrow key on your keyboard - typically twice - until a block of text just before this is selected: the text that begins with <code>div id="qrcode"</code>:</p>
<p><img alt="" height="239" src="https://lh5.googleusercontent.com/PYt5C4-hcdZlxiw9PY0DTGnwDqplKJy0SVXn-GAItqZ54lONLPFz0joCgu4IhEKkIkMtr51esAjYUjDMKtDRvdzS9RnoQd0RmyMhoh065rZnx_Yeg6r9WaZU_9eg5HwZ5K1pWkKg" width="559"></p></li>
<li>Within that block of text, select and copy the "secret" text to the Clipboard. That's the text immediately following <code>secret=</code> and ending before <code>&amp;issuer=</code>, in the token string that begins with <code>otpauth://</code>, in the location shown by <code><strong>secrettexttocopyishere</strong></code> in the example below:
<p><code>otpauth://totp/hpcs%3ATOTP10976BCD?secret=<strong>secrettexttocopyishere</strong>&amp;issuer=Lawrence%20Berkeley%20National%20Laboratory</code></p>
<p>The "secret" text will typically be 32 characters in length, and consist of both uppercase letters and digits.</p>
<p>For example:<br><img alt="" height="312" src="https://lh5.googleusercontent.com/EzVICqtrxdXw_u1xCbVp4FvbIHtdRnBG5y9mPtEbXXWLxeufqVRXgx_yIgJ9-UgYuttn4FJulfpgKMuwc9_jYRGQKx9tXmXgDHaoWtsLzxBiWX47DDNjvdZRJImYe6wPjHarXh-C" width="567"></p>
<p>If you can't easily select just that "secret" text within Chrome's Developer Tools window itself, as an alternative, you can paste in the full token and perhaps even some surrounding text into a text editor or word processing application, and select that text there. (<em>If you do so, for optimum security, do not ever save that token - nor the "secret" text within that token - in any document on your disk.</em>)</p></li>
<li>Paste that "secret" text into the "Enter Code" field in Authy, next to the "Add Account" button.
<p>(If Authy is hidden below your browser window, you can bring it back to the front: from Chrome's menus, select Windows -&gt; Authy.)</p>
<p><em>Be sure to verify that the text pasted into the "Enter Code" field is exactly the same as the "secret" text in the token</em>. (If these differ, even by only a single character, the one-time passwords that Authy generates will not work with Savio.)</p>
<p><img alt="" height="509" src="https://lh5.googleusercontent.com/IN-XIUUF7OvkjhL2xkL9VHMRR0ugT7Bb9eE0qbnGw8boAZOeX651XGX2T3IZN4Tls4vlG3EtHjQoylv9Kffy5xhPEn1CPby-OsF59Q6l1Qi6lS6zoXaPmvRlPwqq7G008BTfPGuR" width="546"></p></li>
<li>Click the "Add account" button.</li>
<li>On the next screen, select a logo for your new Authy account and enter a name for that account. ("Savio" - or any similar name - is a reasonable option for an account name.) Then click "Done".
<p><img alt="" height="505" src="https://lh4.googleusercontent.com/LJYYxbcdSTcxqc_8Sd2PbPWuT_vUY8MNTwQ397EYB65LYneveGGwWWmKJ2Io0quLsP5zg9SiY9SqFPXrAVoVT_E1Pd2EOOAU3EzGrxewtUrWpjDJ5qsi6DWgSZiLOnA9ZomBf3D8" width="545"></p></li>
<li>At the "Your account has been created" prompt, click "Accept".</li>
<li>Click the Close ('x') button at upper right, to move from the Settings screen, to the screen where you can generate one-time passwords.
<p><img alt="" height="103" src="https://lh3.googleusercontent.com/9WL7FvgSiCkoN1OEWe8lukHyHWGJL7rW6RGYnsum4SdIVnk3GOzja1rG0s3vFfk81U-hBqErZXgWywpJeufS2DUXGY666KpzwjZIhXn5HhhOZ9TWXeMe5sWc5biqJLUQ39Mc1d79" width="543"></p></li>
<li>On the screen where you can generate OTPs, click on the logo (or name) for the account you just created:
<p><img alt="" height="198" src="https://lh6.googleusercontent.com/XWHOy0rvUFF3xtROQkcUZMNv0XGTBrRc0HvBD_QJZ975B0dQz94qxr81a-ewNOZtR-Kf4QD-ssbYATs8HGQ4cxOLlVLM9j2ukMELn2iJTRirkse0EHlYwRtHfjwmsXS9_ex_hJjn" width="346"></p></li>
<li>You should now see one-time passwords being generated: a new one will be displayed every 30 seconds:
<p><img alt="" height="237" src="https://lh4.googleusercontent.com/_s-MxYtXYOPQm4nEtDqtw_Ge8AOGHI9od7rhZiTrk2w7r7TZ_ecAXpbVrcUWZIZMm1STEcsSiQhLyuKK2_bUdPc8KfSCN92IWPa4ixCHcPFL1OROdCiWawbbEXYNzjOl1Wy7jmLj" width="345"></p>
<p>(Authy displays the one-time password with a space between the first and last three digits. When you click the "Copy" button, however, the password is correctly copied to the clipboard without that space.)</p></li>
</ol><p>Assuming the "secret" text you pasted into Authy's "Enter Code" field in step 19, above, was the correct text from your token, you've now successfully completed the process of setting up Authy to generate one-time passwords for Savio.</p>

### Logging into Savio

<p>When you want to log into Savio:</p>
<ol><li>Use your terminal or SSH application to connect to <code>hpc.brc.berkeley.edu</code><br>
&nbsp;</li>
<li>At Savio's Password: prompt, enter your token PIN (but don't yet press Return).<br>
&nbsp;</li>
<li>Click Authy's "Copy" button to copy the one-time password to the Clipboard.<br>
&nbsp;</li>
<li>Then, at Savio's Password: prompt, immediately following the token PIN that you've already entered, paste in the one-time password from Authy and press Return.</li>
</ol><p>(For more details on logging in, please see the <a href="{{ site.baseurl }}/services/high-performance-computing/logging-brc-clusters">Logging into Savio</a> documentation.)</p>

### Launching the Authy app

<p>To launch the Authy app later on:</p>
<ol><li>Open the Chrome browser.</li>
<li>In Chrome, visit <code>chrome://apps</code></li>
<li>From the Apps page, open the Authy icon.</li>
<li>Enter your Master password, and click "Unlock".</li>
<li>You should now see the screen where you can generate one-time passwords. Click on the logo (or name) of your account, to start generating new one-time passwords.</li>
</ol>
