---
title: Faculty Computing Allowance
keywords: 
tags: [hpc]
---

# Faculty Computing Allowance

Download the&nbsp;<a href="{{ site.baseurl }}/pdf/FCA%20Bundle.pdf" target="_blank">Faculty Computing Allowance info packet</a>.

!!! summary 
    The Faculty Computing Allowance (FCA) provides up to 300K&nbsp;<a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/running-your-jobs/service-units-savio">Service Units (SUs)</a>&nbsp;of free compute time per academic year to all qualified UC Berkeley faculty/PIs, where&nbsp;<a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/running-your-jobs/service-units-savio">one SU is equivalent to one compute cycle on the latest standard hardware</a>. (Allowances are prorated based on month of application during the allowance year, which starts on June 1 and ends on May 31 the following year.) PIs can also pool their allowances if their groups are working together; however, this must be specified when the allowance is initially setup or during the annual renewal period. Please note that allowances can be shared but are not transferable. People needing more compute time beyond their allowance should review the&nbsp;<a href="{{ site.baseurl }}/services/high-performance-computing/getting-account/options-when-fca-exhausted">options for getting more computing time for a project</a>. Savio&nbsp;cluster project names for these allocations start with a '<code>fc_</code>’ prefix; for example&nbsp;<code>fc_projectname</code>.

<p dir="ltr">Faculty and PIs wanting to obtain access to an FCA for their research will need to complete the <a href="https://docs.google.com/forms/d/e/1FAIpQLScAyqNioiO7Mave-sKjQc9we8-V_CJK9RHfuM-Ca01_okydmg/viewform" style="text-decoration:none;" target="_blank">Requirements Survey</a>&nbsp;and include an initial list of anticipated users, as indicated on that form. A Savio-cluster specific project will be set up using a unique name that consists of a single word (without any spaces between characters)&nbsp;and all user accounts will be created&nbsp;under that project name. When choosing a (single-word) name for an FCA project, the PI should also make sure not to choose something common like "astronomy", "biology", or "genomics" that has likely already been utilized by another research group for their FCA project name. The project name will also&nbsp;be used to set up (SLURM)&nbsp;scheduler&nbsp;accounts, for running computational jobs and tracking cluster usage.</p>
<p>Once a project (and its associated Savio scheduler account) has been created, any UCB researcher who wants to open an additional Savio&nbsp;cluster account, to be associated with that existing project, can do so by submitting the&nbsp;<a href="https://docs.google.com/forms/d/e/1FAIpQLScOiGy3v3ZvxqHAKPiU-GNwA_WmUoUUgXKXPZ1j_5dMOS2m6Q/viewform" style="color: rgb(24, 89, 155);" target="_blank">Additional User Account Request Form</a>. Accounts are added upon approval of the faculty or PI&nbsp;for the project. Usernames must be unique and are allocated on a first-come, first served basis. This means that users may need to choose an alternate username for their accounts, if the one that they originally want has been taken.</p>

!!! danger "Security Alert"
    For security reasons, access to Savio&nbsp;will be through the use of two-factor authentication using Google Authenticator&nbsp;one-time password tokens. This greatly reduces the risk of a security compromise due to stolen user credentials.

<p>As a service provided at no cost to researchers, an FCA does not guarantee access to the full amount of SUs. SUs spent on jobs killed due to unplanned outages cannot be reimbursed.</p>
<p>Please also note that if a researcher is already the PI of an existing FCA project, they can not request the creation of a new FCA account. So, for example, if a researcher has exhausted the allocated service units on their FCA, they should not request the creation of a new FCA, but, rather, they should renew their already existing FCA, purchase additional computing time on Savio, or pursue other &nbsp;<a href="{{ site.baseurl }}/services/high-performance-computing/getting-account/options-when-fca-exhausted">options for getting more computing time for a project</a>.</p>

#### Adding Additional Users to Your Project 

<p>UCB researchers who want to request new user accounts on the cluster, to be associated with an existing project, can&nbsp;submit the&nbsp;<a href="https://docs.google.com/forms/d/e/1FAIpQLScOiGy3v3ZvxqHAKPiU-GNwA_WmUoUUgXKXPZ1j_5dMOS2m6Q/viewform">Additional User Account Request/Modification Form</a>.&nbsp;PIs can also use the Request Form to submit account requests for non-UCB users associated with their project.</p>
<p>All user account requests require the approval of the PI for the relevant project on the cluster, and are added on a first come, first served basis.</p>
<p>Note: each first-time user will also need to complete a&nbsp;<a href="{{ site.baseurl }}/services/high-performance-computing/getting-account/user-access-agreement-form">User Access Agreement Form</a>. (For convenience, this access agreement form is linked from the user account request form, above.)</p>

#### Renewing Your Faculty Computing Allowance

<p>Each year, beginning in May, you can submit an <a href="https://docs.google.com/forms/d/e/1FAIpQLScjSrudmwrixwrSB53N9afIvjzTbwUbisFOQYz15G_mGAW9vQ/viewform" target="_blank">application form</a> to renew your Faculty Computing Allowance. The renewal form is considerably simpler and briefer than <a href="https://docs.google.com/forms/d/e/1FAIpQLScAyqNioiO7Mave-sKjQc9we8-V_CJK9RHfuM-Ca01_okydmg/viewform" target="_blank">the form</a> used to request a first-time Allowance.</p>
<p>Links to this renewal <a href="https://docs.google.com/forms/d/e/1FAIpQLScjSrudmwrixwrSB53N9afIvjzTbwUbisFOQYz15G_mGAW9vQ/viewform" target="_blank">application form</a> are typically emailed to Allowance recipients (and to other designated "main contacts" on such accounts) by or before mid-May each year. (There are often some at least modest differences in the renewal application process from year to year, so there is no permanent online location for this form.)</p>

!!! note
    Renewal applications submitted during May will be processed beginning June 1st. Those submitted and approved later in the year, after the May/June period (i.e. after June 30th), will receive pro-rated allowances of computing time, based on month of application during the allowance year. Note that new allowances that are set up in June or old ones that are renewed in June are the only ones that get the full 300,000 SUs allocation.

#### Viewing the Usage of Your Allowance

<p>You can view how many Service Units have been used to date under a Faculty Computing Allowance, or by a particular user account on Savio, via the <a class="toc-filter-processed" href="{{ site.baseurl }}/services/high-performance-computing/getting-help/frequently-asked-questions#q-how-can-i-check-on-my-faculty-computing-allowance-fca-usage-">check_usage.sh script</a>.</p>

#### Charges for Running Jobs

<p>Each time a computational job is run on the cluster under a&nbsp;Faculty Computing Allowance account, charges are incurred against that account, using abstract measurement units called <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/running-your-jobs/service-units-savio">Service Units (SUs)</a>. Please see&nbsp;<a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/running-your-jobs/service-units-savio">Service Units on Savio</a> to learn more about how these charges are calculated.</p>

#### Getting More Compute Time When Your Allowance Is Used Up

<p>Have you exhausted your entire Faculty Computing Allowance? There are a number of <a href="{{ site.baseurl }}/services/high-performance-computing/getting-account/options-when-fca-exhausted">options for getting more computing time for your project</a>.</p>

#### Acknowledgements

<p>Please acknowledge Savio in your publications. (Such acknowledgements are an important factor in helping to justify ongoing funding for, and expansion of, the cluster.) A sample statement is:</p>
<p><em>"This research used the Savio computational cluster resource provided by the Berkeley Research Computing program at the University of California, Berkeley (supported by the UC Berkeley Chancellor, Vice Chancellor for Research, and Chief Information Officer)."</em></p>
<p>As well, we encourage you to <a href="https://docs.google.com/forms/d/e/1FAIpQLSdu-HaywUKiIqfZtQ8vLzc6sb3ZeVh-qgpbvXBXvngVPatsZA/viewform" target="_blank">tell us how BRC impacts your research</a>&nbsp;(Google Form), at any time!</p>
