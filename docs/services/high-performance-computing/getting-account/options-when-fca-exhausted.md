---
title: Options When a Faculty Computing Allowance Is Exhausted
keywords: 
tags: [hpc]
---

# Options When a Faculty Computing Allowance is Exhausted 

!!! summary
    When you’ve used up your <a href="{{ site.baseurl }}/services/high-performance-computing/getting-account/faculty-computing-allowance">Faculty Computing Allowance</a> of computing time on the campus’s Savio high performance computing cluster, but still need more time on Savio (or an equivalent computing resource) to complete your project, here are some of the options that you might now consider:
    <ol>
        <li><a href="#purchase-savio" class="toc-filter-processed">Purchase additional computing time on Savio</a></li>
        <li><a href="#renew-fca" class="toc-filter-processed">Apply to renew your Faculty Computing Allowance, during the next application period</a></li>
        <li><a href="#contribute-condo" class="toc-filter-processed">Contribute Condo nodes to Savio</a></li>
        <li><a href="#obtain-hpc-at-uc" class="toc-filter-processed">Obtain an allocation on a UC-wide (HPC@UC) resource</a></li>
        <li><a href="#obtain-xsede" class="toc-filter-processed">Obtain an allocation on a national computing (XSEDE) resource</a></li>
        <li><a href="#purchase-commercial" class="toc-filter-processed">Purchase computing time from a commercial cloud provider</a></li>
    </ol>
    
    To explore any of these options in more depth, please <a href="{{ site.baseurl }}/services/high-performance-computing/getting-help">contact us</a>, and let's begin the conversation!

## <a name="purchase-savio" id="purchase-savio"></a><strong>1. Purchase additional computing time on Savio</strong>

<p>Via a Memorandum of Understanding (MOU) and a one-time payment using a campus chartstring, you can pay for one or more additional blocks of compute time at an introductory rate of one cent ($0.01) per <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/running-your-jobs/service-units-savio">Service Unit (SU)</a>. One SU equals one core hour of compute time on a standard compute node; <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/running-your-jobs/service-units-savio">it can vary for other types of nodes</a>.</p>
<p>For instance, if you originally received a Faculty Computing Allowance of 300K SUs, and wished to purchase another, equivalent block of time, you can do so for $3,000. There is a minimum purchase of 50K SUs, at a cost of $500.00.</p>
<p>This compute time does not expire; it will be carried over until it is used up, and can be used alongside any additional no-cost Allowance(s) granted in the future.</p>
<p>To make your purchase, please <a href="{{ site.baseurl }}/services/high-performance-computing/getting-help" target="_blank">contact us</a> via email at <a href="mailto:brc-hpc-help@berkeley.edu?subject=Purchase%20of%20computational%20time%20on%20Savio%20via%20MOU">brc-hpc-help@berkeley.edu</a> with your responses to the following questions:</p>
<ol><li>Name of the faculty member or PI?</li>
<li>Number of Service Units requested? (E.g., 100K, 300K ..?)</li>
<li><span class="m_-6955682788859526793gmail-tabs2_section m_-6955682788859526793gmail-tabs2_section_1 m_-6955682788859526793gmail-tabs2_section1" id="m_-6955682788859526793gmail-section_tab.991f88d20a00064127420bc37824d385" style="display:block"><span class="m_-6955682788859526793gmail-section" id="m_-6955682788859526793gmail-section-991f88d20a00064127420bc37824d385"><span class="m_-6955682788859526793gmail-sn-widget-textblock-body m_-6955682788859526793gmail-sn-widget-textblock-body_formatted m_-6955682788859526793gmail-ng-binding">Campus chartstring to bill?</span></span></span></li>
<li><span class="m_-6955682788859526793gmail-tabs2_section m_-6955682788859526793gmail-tabs2_section_1 m_-6955682788859526793gmail-tabs2_section1" style="display:block"><span class="m_-6955682788859526793gmail-section"><span class="m_-6955682788859526793gmail-sn-widget-textblock-body m_-6955682788859526793gmail-sn-widget-textblock-body_formatted m_-6955682788859526793gmail-ng-binding">Type of account represented by chartstring? (E.g., is this a contract or grant? What is the funding agency or campus source?)</span></span></span></li>
<li>Name and email address of a departmental business contact, for correspondence about the chartstring?</li>
<li>Handled the same as the Faculty Computing Allowance? Or with changes? (E.g., a change in which user accounts should have access to it?)</li>
<li>Date (or date range) by which this new block of computational time needs to be made available?</li>
</ol>

## <a name="renew-fca" id="renew-fca"></a><strong>2. Apply to renew your Faculty Computing Allowance, during the next application period</strong>

<p>Each year, beginning in May, you can submit an application to renew your Faculty Computing Allowance. Links to the renewal application form are typically emailed to Allowance recipients (and to other designated "main contacts" on such accounts) by or before mid-May.</p>
<p>Renewal applications submitted in May will be processed beginning June 1st. Those submitted and approved later in the year, after either May or June, will receive pro-rated allowances of computing time, based on the application month within the allowance year. Note that new allowances that are set up in June or old ones that are renewed in June are the only ones that get the full 300,000 SUs allocation.</p>

## <a name="contribute-condo" id="contribute-condo"></a><strong>3. Contribute Condo nodes to Savio</strong>

<p>If you expect your computational needs to be large and/or ongoing, another possibility is to purchase compute nodes and contribute them to the Savio Condo.</p>
<p>Purchasing and contributing hardware gives you, as a Condo owner/contributor, the following benefits over the five year life of your contribution:</p>
<ul><li><a href="{{ site.baseurl }}/services/high-performance-computing/getting-help/frequently-asked-questions#q-what-are-the-benefits-of-participating-in-the-condo-cluster-program-">Priority access</a> to compute resources up to the size of your contribution.</li>
<li>The ability to <a href="http://research-it.berkeley.edu/blog/16/01/29/low-priority-queue-allows-condo-contributors-glean-unused-cycles-savio" target="_blank">run larger jobs across even more nodes</a>.</li>
<li>All the extensive infrastructure that Savio provides - including colocation in the campus data center, power, cooling, high-speed networking, a firewalled subnet, login and data transfer nodes, commercial compilers, and a large, fast parallel filesystem - along with professional system administration of this entire infrastructure.</li>
</ul><p>For more information on this option, please see <a href="{{ site.baseurl }}/services/high-performance-computing/condos/condo-cluster-service">Condo Cluster Program</a>.</p>

## <a name="obtain-hpc-at-uc" id="obtain-hpc-at-uc"></a><strong>4. Obtain an allocation on a UC-wide (<a href="http://research-it.berkeley.edu/blog/17/01/23/hpcuc-provides-researchers-rapid-access-supercomputing" target="_blank">HPC@UC</a>) resource</strong>

<p>The <a href="http://research-it.berkeley.edu/blog/17/01/23/hpcuc-provides-researchers-rapid-access-supercomputing" target="_blank">HPC@UC program</a> offers no-cost computing time on computational resources at the San Diego Supercomputer Center (SDSC) to University of California-affiliated researchers. At least one of these, Comet, offers a software environment which is fairly similar to Savio's, reducing the effort required to replicate your current setup there.<br><br>(Please note that you can access SDSC's Comet resource via either the <a href="mailto:HPC@UC">HPC@UC</a> program or XSEDE program (below) - but not both - so we encourage you to <a href="{{ site.baseurl }}/services/high-performance-computing/getting-help">contact us</a> for assistance before applying to the <a href="mailto:HPC@UC">HPC@UC</a> program.)</p>

## <a name="obtain-xsede" id="obtain-xsede"></a><strong>5. Obtain an allocation on a national computing (</strong><a href="https://www.xsede.org/" target="_blank">XSEDE</a><strong>) resource</strong>

<p>The NSF-funded <a href="https://www.xsede.org/" target="_blank">XSEDE program</a> offers no-cost computing time on computational centers across the country. Several of these offer clusters whose software environments are fairly similar to Savio's, reducing the effort required to replicate your current setup there.</p>
<p>Applications typically are reviewed quarterly, and on approval, access will be granted at some point thereafter. There are also options for getting up and running much more quickly, albeit with smaller blocks of compute time. For instance, startup and/or trial allocations allow you to install and test your software on a cluster; these offer relatively fast approval and set up, and you can apply for them at any time.</p>
<p>UC Berkeley has an <a href="http://research-it.berkeley.edu/services/cloud-computing-support/overview-cloud-computing-support" target="_blank">XSEDE Campus Champion</a> who can work with you to help you find and apply for XSEDE computing resources that best fit your needs. We can also help you to get your software working on Savio, to demonstrate readiness to take advantage of XSEDE resources.</p>

## <a name="purchase-commercial" id="purchase-commercial"></a><strong>6. Purchase computing time from a commercial cloud provider</strong>

<p>Amazon, Microsoft, and Google are among a number of commercial vendors offering computing and storage services that you can obtain with relatively little lead time. Several of these services offer computing environments somewhat similar to Savio’s, along with others that use very different computing paradigms.</p>
<p>Berkeley Research Computing (BRC) offers a <a href="http://research-it.berkeley.edu/services/cloud-computing-support" target="_blank">Cloud Computing Support consulting service</a>, which can help you determine whether one or more commercial cloud providers’ offerings might be a good match for your computational work. BRC consultants will also help you apply for vendor grants, where available, that can help defray at least some of your costs.</p>
<p>You can choose - and mix and match - any combination of these options: an MOU for purchasing additional computing time on Savio, a new Faculty Computing Allowance, to be applied for during the next Allowance period; contributing Condo nodes; an allocation on an <a href="http://research-it.berkeley.edu/blog/17/01/23/hpcuc-provides-researchers-rapid-access-supercomputing" target="_blank">HPC@UC</a> or <a href="https://www.xsede.org/" target="_blank">XSEDE</a> facility; and/or purchasing services from a commercial cloud provider.</p>
<p>If you're interested in exploring one or more of these options, please <a href="{{ site.baseurl }}/services/high-performance-computing/getting-help">let us know</a> and let's begin the conversation!</p>
