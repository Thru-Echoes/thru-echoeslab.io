---
title: Getting an Account
keywords: 
tags: [hpc]
---

# Getting an Account 

!!! summary

    The <strong>Savio</strong> cluster is open to all UC Berkeley PIs who need access to high performance computing. Multi-institutional research collaborations involving at least one UC Berkeley PI are also welcome. PIs can then designate researchers (including those not affiliated with UC Berkeley) working on the project to be users on Savio. Instructors of record can get access to Savio for their course through an Instructional Computing Allowance.

    Each individual user must be associated with a project or course. If the project or course already exists, you can
    <a href="#Getting-User-Accounts">request a new user account or request access  to an additional project using your existing account</a>.
    
<p><a name="Obtaining-Access" id="Obtaining-Access"></a></p>

### Obtaining Access to Savio for a Project or Course

<p>There are three primary ways for a PI or course instructor to obtain access to Savio for a project or course:</p>
<ol><li>For projects: requesting a block of no-cost computing time via a Faculty Computing Allowance (FCA). This option is currently offered to UC Berkeley faculty, who can then share their Allowance amongst their postdocs, grad students, staff researchers, and other campus research affiliates. See below for more on eligibility. For details, please see <a href="{{ site.baseurl }}/services/high-performance-computing/getting-account/faculty-computing-allowance">Faculty Computing Allowance</a>. To request an FCA, please fill out the <a href="https://docs.google.com/forms/d/e/1FAIpQLScAyqNioiO7Mave-sKjQc9we8-V_CJK9RHfuM-Ca01_okydmg/viewform" target="_blank">Savio access request form</a>.</li>
<li>For projects: purchasing and contributing Condo nodes to the cluster. This option is open to any campus PI, and provides ongoing, priority access to you and your research affiliates who are members of the Condo. For details, please see <a href="{{ site.baseurl }}/services/high-performance-computing/condos/condo-cluster-service">Condo Cluster Service</a>.</li>
<li>For courses: requesting an Instructional Computing Allowance (ICA), which provides a block of no-cost computing time for an instructor and students in a course. For details, please see <a href="{{ site.baseurl }}/services/high-performance-computing/getting-account/instructional-computing-allowance">Instructional Computing Allowance</a>. To request an ICA, please fill out the <a href="https://docs.google.com/forms/d/e/1FAIpQLSeNO3sw3YSPYarL70Bd74kz6Wl3rwyXFFtUVU08zryfMSJh_A/viewform" target="_blank">ICA request form</a>.</li></ol>
<p>If you'd like to further discuss any&nbsp;of these options in more detail, or if you have any other questions, you're invited to contact us at <a href="mailto:brc-hpc-help@berkeley.edu">brc-hpc-help@berkeley.edu</a>.</p>
<p>This training video also provides an overview of your options for accessing Savio:</p>
<iframe width="560" height="315" src="https://www.youtube.com/embed/Denj8NyUPVo?start=134&end=209" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<p><a name="Getting-User-Accounts" id="Getting-User-Accounts"></a></p>

### Getting a User Account

<p>After a new project (FCA or Condo) has been established, UC Berkeley researchers who want to request new user accounts on the cluster, to be associated with an existing project or projects, can submit the <a href="https://docs.google.com/forms/d/e/1FAIpQLScOiGy3v3ZvxqHAKPiU-GNwA_WmUoUUgXKXPZ1j_5dMOS2m6Q/viewform" target="_blank">Additional User Account Request/Modification Form</a>. PIs can also use the Request Form to submit account requests for non-UCB users associated with their project. Users wishing to get access to an additional project (FCA or condo) in addition to their current project(s) should also fill out the form.</p>
<p>All user account requests require the approval of the PI for the relevant project on the cluster.</p>
<p>Usernames must be unique and are allocated on a first-come, first served basis. This means that users may need to choose an alternate username for their accounts, if the one that they originally want has been taken.</p>
<p>Note: each new user will also need to complete a <a href="{{ site.baseurl }}/services/high-performance-computing/getting-account/user-access-agreement-form">User Access Agreement Form</a>. (For convenience, this access agreement form is linked from the user account request form, above.)</p>
<p>Once the User Access Agreement Form has been submitted and the new BRC user account has been set up on the Savio cluster, the new user will receive an email shortly thereafter confirming this and pointing them to instructions on how they can access their new BRC user account.</p>
<p><a name="Accessing-User-Accounts" id="Accessing-User-Accounts"></a></p>
<p>New users should also keep in mind that there will be a processing wait time of a few days or so between the time they request the creation of a user account and the time they receive the confirmation email pointing them to instructions on how they can access their new BRC user account. Users should not attempt to follow any of these instructions (e.g., linking their personal account to their BRC HPC Cluster account, installing and setting up Google Authenticator, setting up their one-time password, etc., and logging into the BRC Cluster) before they have received the confirmation email.</p>

### Accessing User Accounts

<p>Please see the <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/logging-brc-clusters">Logging In</a> page for details regarding access to BRC user accounts. For security reasons, all access is via One-Time Passwords (OTPs), to reduce the risk of a compromise from stolen user credentials.</p>
<p><a name="Closing-User-Accounts" id="Closing-User-Accounts"></a></p>

### Closing User Accounts

<p>The PI for the project is ultimately responsible for notifying Berkeley Research Computing when user accounts should be closed, and to specify the disposition of a user's software, files, and data.</p>
<p>For ICAs, all student accounts are closed and files are deleted at the end of the semester when the ICA expires, unless the student accounts are also associated with an FCA or condo.</p>
<p>For FCAs and condos, users’ accounts are not automatically deactivated upon termination of an employee because many people change their employment status, but remain engaged with a research project.</p>
<p>As well, in some cases users share software and data from their home directory, and others may depend on these. For these reasons, account terminations have to be requested by the PI, or the user of the account.</p>
