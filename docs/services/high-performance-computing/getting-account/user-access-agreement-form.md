---
title: User Access Agreement Form
keywords:
tags: [hpc, accounts] 
---

# User Access Agreement Form

Every prospective user of the Savio high-performance computing cluster and other clusters managed by Berkeley Research Computing (BRC) must first complete and (electronically) sign a <strong>BRC User Access Agreement form</strong>, in order to obtain credentials to access the cluster.
<div>&nbsp;</div>

#### With CalNet ID

<strong>If you have a UC Berkeley CalNet ID</strong>, please complete <a data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://docs.google.com/a/berkeley.edu/forms/d/e/1FAIpQLSePVig8h1WZmPaUddZycygJdsBUKqvJuiTOdstQiWGDuY8qDA/viewform&amp;source=gmail&amp;ust=1478118128411000&amp;usg=AFQjCNEge-w-xzoV9Yve3WK2hF9b-QLhjw" href="https://docs.google.com/a/berkeley.edu/forms/d/e/1FAIpQLSePVig8h1WZmPaUddZycygJdsBUKqvJuiTOdstQiWGDuY8qDA/viewform" target="_blank">this form</a>.

!!! danger "Attention"
    If you have a <code>berkeley.edu</code> email address, this is the form you should complete. Also, if you encounter a "You need permission" error, try opening the form in an Incognito / Private window in your browser.

#### Without CalNet ID

<strong>If you do not have a CalNet ID</strong>, please complete <a data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://docs.google.com/a/berkeley.edu/forms/d/e/1FAIpQLScKwiXeRwkCmWl9mFu1RvCvWVklv6Acqx7xkCvJpXogQMfU_A/viewform&amp;source=gmail&amp;ust=1478118128411000&amp;usg=AFQjCNEYYqkb99qMdubadCaNg5UvFt3mSQ" href="https://docs.google.com/a/berkeley.edu/forms/d/e/1FAIpQLScKwiXeRwkCmWl9mFu1RvCvWVklv6Acqx7xkCvJpXogQMfU_A/viewform" target="_blank">this form.</a>

!!! danger "Attention"
    Do NOT&nbsp;fill this second form if&nbsp;you have a&nbsp;<code style="font-size: 16px;">berkeley.edu</code>&nbsp;email address. Fill this second form ONLY if you do not have a CalNET ID or else your entry will not be valid.

