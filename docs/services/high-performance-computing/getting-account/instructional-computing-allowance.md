---
title: Instructional Computing Allowance
keywords: 
tags: [hpc]
---

# Instructional Computing Allowance

<p>Instructional Computing Allowances (ICAs) provide a no-cost allocation on the Savio high-performance computing cluster to instructors who need significant computational resources for their courses.</p>
<p><strong>Instructional Computing Allowances</strong> are intended for any instructor of record of an official UC Berkeley course. Each Instructional Computing Allowance is based upon a partnership agreement between BRC and the instructor. The agreement specifies a designated Point of Contact (POC) for the course (usually a GSI or staff member) who is familiar with high performance computing in Savio, and who handles software installation and all student troubleshooting and support. The designated point of contact can escalate issues to BRC staff, and work with BRC staff on software installation and account provisioning. This model provides Savio access to students learning computationally-intensive research techniques and ensures that BRC support staff are not overwhelmed..</p>

#### Allowance Requests 

To apply for an allocation, please <a href="https://docs.google.com/forms/d/e/1FAIpQLSeNO3sw3YSPYarL70Bd74kz6Wl3rwyXFFtUVU08zryfMSJh_A/viewform" target="_blank">fill out the ICA request form</a>. If you have any questions, please contact us at <a href="mailto:brc@berkeley.edu">brc@berkeley.edu</a>.
