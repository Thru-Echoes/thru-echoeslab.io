---
title: Condo Partner Access
keywords: 
tags: [hpc] 
---

# Condo Partner Access

<p>By becoming a Condo Partner, through purchasing and contributing compute nodes to the Savio cluster via the <a href="{{ site.baseurl }}/services/high-performance-computing/condos/condo-cluster-service">Condo Cluster Service</a>, researchers and their groups obtain priority - and hence nearly unlimited - access to resources equivalent to their contribution.</p>
<p>In addition, Condo partners can access any of the other resources in the cluster by using the no-cost <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/running-your-jobs#low-priority">low priority queue</a>. (Such jobs are subject to preemption.)</p>

#### Accessing Savio for Condo partners

<p>For information on obtaining additional user accounts and closing these accounts, please see <a href="{{ site.baseurl }}/services/high-performance-computing/getting-account">Getting an Account</a>.</p>
<p>Important details for Condo partners on how to submit jobs to the cluster (such as the names of scheduler queues and scheduler policies for each Condo) can be found in our information on <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/running-your-jobs" class="toc-filter-processed">running jobs</a>.</p>
