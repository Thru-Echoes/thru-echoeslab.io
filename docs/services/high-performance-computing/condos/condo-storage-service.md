---
title: BRC Condo Storage Service for Savio
keywords: high performance computing, berkeley research computing, status, announcements
tags: [hpc]
---

# Condo Storage Service

!!! summary 
    Berkeley Research Computing (BRC) offers a Condo Storage service for researchers who are <a href="{{ site.baseurl }}/services/high-performance-computing/condos/condo-cluster-service">Savio Condo Cluster</a> contributors and need additional persistent storage to hold their data sets while using the Savio cluster. Researchers pay a one-time upfront cost for this storage, which is available in 42&nbsp;TB increments at a cost of $6210&nbsp;each (plus sales tax), with a typical initial purchase of 84&nbsp;TB for $12.4K (plus tax). The storage is maintained by BRC for a 5-year period thereafter at no additional cost to the researcher. Please feel free to <a href="#Purchasing" class="toc-filter-processed">contact us</a> with your questions about this offering.

## Overview 

<p>By default, each user on Savio is entitled to a 10 GB home directory which receives regular backups; in addition, each Condo-using research group receives up to 200 GB of project space to hold research specific application software shared among the group's users. All users also have access to the large Savio high performance <a href="http://research-it.berkeley.edu/blog/15/07/10/savio-cluster-storage-quadrupled-support-big-data-research" target="_blank">scratch filesystem</a> for working with non-persistent data.</p>
<p>This Condo Storage service supplements the above offerings. It is intended to provide a cost-effective, persistent storage solution for users or research groups that need to import and store large data sets over a long period of time&nbsp;to support their use of Savio. (Note that data stored in Condo Storage should be copied to scratch when actually carrying out your compute.) The table below compares the cost of various storage alternatives and shows the cost advantage of this service:</p>
<h3>Cost comparison for 42&nbsp;TB over 5 yrs</h3>
<table><tbody><tr style="background-color:#d0e0e3"><th>Model/service</th>
<th>Details of cost</th>
<th style="text-align:right">Total cost</th>
<th style="text-align:right">Cost/TB/yr</th>
</tr><tr><td><strong>UCB IST Performance tier</strong></td>
<td>42TB x $1680/TB/yr x 5 yrs</td>
<td style="text-align:right"><strong>$352,000</strong></td>
<td style="text-align:right"><strong>$1680</strong></td>
</tr><tr><td><strong>UCB IST Utility tier</strong></td>
<td>42TB x $600/TB/yr x 5 yrs</td>
<td style="text-align:right"><strong>$126,000</strong></td>
<td style="text-align:right"><strong>$600</strong></td>
</tr><tr><td><strong>CASS (UCLA)<sup>1</sup></strong></td>
<td>42TB x $119.12/TB/yr x 5 yrs</td>
<td style="text-align:right"><strong>$25,015</strong></td>
<td style="text-align:right"><strong>$119</strong></td>
</tr><tr><td><strong>AWS Glacier<sup>2</sup></strong></td>
<td>42TB x $84/TB/yr x 5 yrs</td>
<td style="text-align:right"><strong>$17,640</strong></td>
<td style="text-align:right"><strong>$84</strong></td>
</tr><tr style="background-color:#d9ead3"><td><strong>BRC Condo Storage</strong></td>
<td>$6210 + CA Sales tax: $329<sup>3</sup></td>
<td style="text-align:right"><strong>$6539</strong></td>
<td style="text-align:right"><strong>$31</strong></td>
</tr></tbody></table>

## Program Detail

<p>Similar in concept to the Condo Computing program, BRC provides the storage infrastructure consisting of a Dell Compellent SC8000/FS8600 storage server and covers the cost of supporting the system. Researchers can purchase Compellent storage shelves and have them hosted in the BRC storage infrastructure. This allows researchers to 1) pay a one-time upfront cost for the storage without having to incur ongoing support costs; and 2) pay only for the incremental cost of the storage shelf and disk drives because the cost of the storage servers and disk controllers are covered by BRC.</p>
<p>Storage shelves are purchased and maintained on a 5-year life cycle. At the end of 5 yrs, Condo Storage contributors who wish to continue participating in the program will be offered one of the following options, and possibly both: (1) purchase new storage; or (2) cover the cost of annual vendor hardware maintenance for their existing storage shelves. Which of these options are available will depend on when the contributor’s storage was purchased, and when the BRC Program determines that server hardware for the storage infrastructure must be upgraded.</p>

## Hardware 

<p>The BRC storage infrastructure consists of a Dell Compellent SC8000/FS8600 enterprise storage server with dual controllers and SC200 storage shelves containing 12x10TB Nearline SAS 7200 RPM disks in a RAID6 configuration. It is connected to the BRC 10GBE firewalled subnet supporting the Savio cluster and is only accessible from outside the Savio cluster via the BRC Data Transfer Node which in turn is connected to the UC Berkeley Science DMZ network. Performance is very good, but users whose computation includes heavy I/O should stage data on the parallel filesystem mounted at /global/scratch, rather than access condo storage directly from Savio's computational nodes. PIs can purchase a fully loaded Compellent SC200 storage shelf for $12.4K (plus sales tax) that will yield 84TB usable storage. Half shelves yielding 42&nbsp;TB usable storage are also available for $6210&nbsp;(plus tax).</p>

## Backups 

<p>Backups are the responsibility of the owner of the storage. Faculty can work with BRC and IST staff to obtain regular backups or to set up regular filesystem snapshots. </p>

## Testing 

<p>Users who wish to test the performance of Condo Storage can run their tests using their home or group directories, which use the same hardware. We request that users copy their data to /global/scratch to perform computation, especially computation with heavy I/O usage, rather than running it directly on Condo Storage.</p>

## Purchasing

<p>Interested faculty and PIs should contact us at&nbsp;<a href="mailto:brc-hpc-help@berkeley.edu">brc-hpc-help@berkeley.edu</a> for further information. BRC will assist with entering a storage shelf purchase requisition in increments of 42TB, and initiate the procurement. The entire process from start to production is anticipated to be 6 weeks.</p>

## Eligibility 

<p>The Condo Storage service is available only to UC Berkeley faculty who are Savio <a href="{{ site.baseurl }}/services/high-performance-computing/condos/condo-cluster-service">Condo Cluster Service</a> contributors.</p>

<hr><p><sup>1</sup>Cloud Archival Storage Service (<a href="https://idre.ucla.edu/cass" target="_blank">https://idre.ucla.edu/cass</a>). Included for price comparison only, as CASS would not be performant enough for many Savio use-cases. Purchase w/ federal funds incurs a 26% overhead.</p>
<p><sup>2</sup>Based upon current AWS pricing of $.007/GB/mo; does not include data egress charges. Included for price comparison only, as Glacier would not be performant enough for many Savio use-cases.</p>
<p><sup>3</sup>Reflects <a href="http://supplychain.berkeley.edu/buying/ca-partial-sales-tax-rate-exemption" target="_blank">CA Partial Sales Tax Rate Exemption</a> for equipment purchased for research and development in biotechnology, physical, engineering, and life sciences.</p>

