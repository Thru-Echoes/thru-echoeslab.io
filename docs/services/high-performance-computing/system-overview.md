---
title: System Overview
keywords: 
tags: [hpc]
---

# System Overview

!!! summary
    The <strong>Savio</strong> institutional cluster forms the foundation of the Berkeley Research Computing (BRC) Institutional/Condo Cluster. As of April 2020, the system consists of 600 compute nodes in various configurations to support a wide diversity of research applications, with a total of over 15,300 cores and a peak performance of 540 teraFLOPS (CPU) and 1 petaFLOPS (GPU), and offers approximately 3.0 Petabytes of high-speed parallel storage. Savio was launched in May 2014, and is named after Mario Savio, a political activist and a key member of the Berkeley Free Speech Movement.


<!--break--><p>
<img alt="" src="{{ site.baseurl }}/images/SAVIO%20Overview.jpeg" style="height:464px; width:875px"></p>
<p>The video below describes the difference between login, compute, and data transfer nodes:</p>
<iframe width="560" height="315" src="https://www.youtube.com/embed/Denj8NyUPVo?start=211&end=252" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<a name="Compute" id="Compute"></a>

## Compute

<p><img alt="" src="{{ site.baseurl }}/images/DellBlade.jpg" style="float:right; height:122px; width:162px"><strong>savio1</strong>: Savio1&nbsp;is the first generation of compute nodes purchased in 2014.&nbsp;Each node is a Dell PowerEdge C6220 server blade equipped with two Intel Xeon 10-core Ivy Bridge processors (20 cores per node) on a single board configured as an SMP unit. The core frequency is 2.5 GHz and supports 8 floating-point operations per clock period with a peak performance of 20.8 GFLOPS/core or 400 GFLOPS/node. Turbo mode enables processor cores to operate at 3.3 Ghz when sufficient power and cooling are available.</p>
<ul style="margin-left: 40px;"><li>savio: 160 nodes with 64 GB of 1866 Mhz DDR3 memory</li>
<li>savio_bigmem: 4 nodes are configured as "BigMem" nodes with 512 GB of 1600 Mhz DDR3 memory.</li></ul>
<p><strong>savio2:</strong> Savio2 is the 2nd generation of compute for the Savio cluster&nbsp;purchased in 2015.&nbsp;The general compute pool consists of Lenovo NeXtScale nx360m5 nodes equipped with two Intel Xeon 12-core Haswell processors (24 cores per node) on a single board configured with an SMP unit. The core frequency is 2.3 Ghz and supports 16 floating-point operations per clock period. A small number of nodes have slightly different processor speeds and some have 28 cores per node.</p>
<p><ul style="margin-left: 40px;"><li>savio2: 163 nodes with 64 GB of 1866 Mhz DDR3 memory</li>
<li>savio2_bigmem: 20 nodes are configured as "BigMem" nodes with 128 GB of 2133 Mhz DDR4 memory.</li></ul></p>
<p><strong>savio2_htc</strong>: Purchased as part of the 2nd generation of compute for Savio, the HTC pool is available for users needed to do High-Throughput Computing or serial compute jobs. The pool&nbsp;consists of 20 ea. Lenovo NeXtScale nx360m5 nodes equipped with faster 3.4 Ghz clock speed, but fewer - 12 instead of 24 -&nbsp;core count Intel Haswell processors and 128 GB of 2133 Mhz DDR4 memory&nbsp;to better facilitate serial workloads.</p>
<p><strong>savio2_gpu</strong>: Also purchased as part of the 2nd generation of compute for Savio, the Savio2 GPU nodes&nbsp;are intended for researchers using graphics processing units (GPUs) for computation or image processing. The pool consists of 17 ea. Finetec Computer&nbsp;Supermicro 1U nodes, each equipped with 2 ea. Intel&nbsp;Xeon 4-core 3.0 Ghz Haswell processors (8 total) and 2 ea. Nvidia K80 dual-GPU accelerator boards (4 GPUs total per node). Each GPU offers 2,496 NVIDIA CUDA cores.</p>
<p><strong>savio2_1080ti</strong>: This partition has 7 nodes with (4) nVIDIA GTX 1080Ti GPU cards on each node.&nbsp;&nbsp;</p>
<p><strong>savio2_bigmem</strong>: This partition has nodes with 128GB of RAM</p>
<p><strong>savio2_knl</strong>: This partition of Savio2 has&nbsp;32 nodes with Intel Knights Landing.&nbsp; These nodes have 64-cores in a single socket.&nbsp; The KNL nodes are special in that it has 16 GB of MCDRAM within the processor, with a bandwidth&nbsp;of 400 GB/s.&nbsp; The MCDRAM are typically configured in cache mode, contact support if you need a different mode.&nbsp;&nbsp;&nbsp;&nbsp;</p>
<p><strong>Savio3</strong>: Savio3 is the newest cluster that began at the end of 2018.&nbsp; Its current composition is:</p>
<ul style="margin-left: 40px;"><li>savio3: 116 nodes with Skylake processor (2x16 @ 2.1 GHz).&nbsp; 96 GB RAM</li>
<li>savio3_bigmem: 16 nodes with Skylake processor (2x16 @ 2.1GHz).&nbsp; 384&nbsp;GB RAM.</li>
<li>savio3_xlmem: 2 nodes with Skylake processor (2x16 @2.1 GHz). 1.5 TB RAM</li>
<li>savio3_gpu: 1 node with two nVIDIA Tesla V100 GPU.</li>
<li>savio3_2080ti: 5 nodes with four nVIDIA GTX 2080ti GPUs and 3 nodes with 8 nVIDIA GTX 2080ti GPUs.</li>
</ul><p>&nbsp;</p>
<p>More details about these nodes are available in the <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/hardware-config">Savio hardware configuration</a>.</p>
<p>&nbsp;</p>
<p><a name="Login_Nodes" id="Login_Nodes"></a></p>

## Login Nodes 

<p>When users login to Savio, they will ssh to hpc.brc.berkeley.edu. This will&nbsp;make a connection through the BRC firewall and they will&nbsp;land on one of the 4 interactive Login Nodes. Users should use the Login Nodes to prepare their jobs to be run - editing files and compiling applications, etc. - and for submitting their jobs via the scheduler.&nbsp;These 4 Login Nodes have the same specifications as some of the general compute nodes, but the Login Nodes are shared and are not to be used for directly running computations (instead, submit your jobs via the scheduler, to be run on one or more of the cluster's hundreds of <a href="#Compute" class="toc-filter-processed">Compute Nodes</a>), nor for transferring data (instead, use the cluster's <a href="#Data_Transfer" class="toc-filter-processed">Data Transfer Node</a>).</p>
<p><a name="Storage" id="Storage"></a></p>

## Storage 

<p>The cluster uses a two-tier storage strategy. Each user is allotted 10 GB of backed up&nbsp;home directory storage which is hosted by IST's storage group on Network Appliance NFS servers. These servers provide highly available and reliable storage for users to store their persistent data needed to run their compute jobs. A second storage system available to users is a high performance 885 TB Lustre parallel filesystem, provided by a Data Direct Networks Storage Fusion Architecture 12KE storage platform,&nbsp;that is available as Global Scratch. Users with computations that have moderate to heavy I/O storage demands should use Global Scratch when needing to write and read temporary files during the course of a job.&nbsp;There are no&nbsp;limits on the use of Global Scratch; however, this filesystem is NOT backed up and it&nbsp;will be periodically purged of older files in order to&nbsp;insure&nbsp;that there is sufficient working space for users running current calculations.</p>
<p><a name="Interconnect" id="Interconnect"></a></p>

## Interconnect

<p>The interconnect for the cluster is a 56Gb/s Mellanox FDR infiniband fabric configured as a two-level fat tree. Edge switches will accommodate up to 24 compute nodes and will have 8 uplinks into the core infiniband switch fabric to achieve a 3:1 blocking factor to provide excellent, cost-effective network performance for MPI and parallel filesystem traffic. A second gigabit ethernet fabric will be used for access to interactive login and compute nodes and for&nbsp;Home Directory storage access.</p>
<p><a name="Data_Transfer" id="Data_Transfer"></a></p>

## Data Transfer 

<p>The data transfer node (DTN) is a BRC server dedicated to performing transfers between data storage resources such as the NFS&nbsp;Home Directory storage and Global Scratch Lustre Parallel Filesystem; and remote storage resources at other sites. High speed data transfers are facilitated by a direct connection to CENIC via UC Berkeley's new&nbsp;Science DMZ network architecture.&nbsp;This server, and not the Login Nodes, should be used for large data transfers so as not to impact the interactive performance for other users on the Login Nodes. Users needing to transfer data to and from Savio should ssh from the Login Nodes or their own computers to the Data Transfer Node, dtn.brc.berkeley.edu, before doing their transfers, or else connect directly&nbsp;to that node from file transfer software running on their own computers.</p>

### File Transfer Software 
<br>
For smaller files you can use Secure Copy (SCP) or Secure FTP (SFTP) to transfer files between Savio and another host. For larger files BBCP and GridFTP provide better transfer rates. In addition, Globus Connect - a user-friendly, web-based tool that is especially well suited for large file transfers - makes GridFTP transfers trivial, so users do not have to learn command line options for manual performance tuning.&nbsp;Globus Connect also does automatic performance tuning and has been shown to perform comparable to - or even better (in some cases) than - expert-tuned GridFTP. The Globus endpoint name for the BRC DTN server is ucb#brc</p>

### Restrictions

In order to keep the data transfer nodes performing optimally for data transfers, we request that users restrict interactive use of these systems to tasks that are related to preparing data for transfer or are directly related to data transfer of some form or fashion. &nbsp;Examples of intended usage would be running Python scripts to download data from a remote source, running client software to load data from a file system into a remote database, or compressing (gzip) or bundling (tar) files in preparation for data transfer. Examples of work that should not be done on the data transfer nodes include running a database on the server to do data processing, or running tests that saturate the nodes' resources. The goal in all this is to maintain data transfer systems that have adequate memory and CPU available for interactive user data transfers.</p>
<p><a name="System_Software" id="System_Software"></a></p>

## System Software

<p>Operating System: Scientific Linux 7<br>
Cluster Provisioning: <a href="http://warewulf.lbl.gov" target="_blank">Warewulf Cluster Toolkit</a><br>
Infinband Stack: OFED<br>
Message Passing Library: OpenMPI<br>
Compiler: Intel, GCC<br>
Job Scheduler: SLURM with <a href="http://go.lbl.gov/nhc" target="_blank">NHC</a><br>
Software management: Environment Modules<br>
Data Transfer: <a href="https://www.globus.org/" target="_blank">Globus Connect</a></p>
<p><a name="Application_Software" id="Application_Software"></a></p>

## Application Software 

<p>See <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/accessing-software">information on the available software on Savio</a> for an overview of the types of application software, tools, and utilities provided on Savio.</p>
